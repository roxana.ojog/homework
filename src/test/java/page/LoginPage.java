package page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage {
    @FindBy(how = How.ID, using = "input-login-username")
    private WebElement userField;

    @FindBy(how = How.ID, using = "input-login-password")
    private WebElement passwordField;

    @FindBy(how = How.ID, using = "login-submit")
    private WebElement loginButton;

    @FindBy(how = How.XPATH, using = "//input[@id='input-login-username']/following-sibling::div")
    private WebElement userErrorElement;

    @FindBy(how = How.XPATH, using = "//input[@id='input-login-password']/following-sibling::div")
    private WebElement passwordElementError;

    @FindBy(how = How.ID, using = "login-error")
    private WebElement generalElementError;

    public String getUsernameErrorText() {
        return userErrorElement.getText();
    }

    public String getPasswordErrorText() {
        return passwordElementError.getText();
    }

    public String getGeneralErrorText() {
        return generalElementError.getText();
    }

    public void login(String username, String password) {
        userField.clear();
        userField.sendKeys(username);

        passwordField.clear();
        passwordField.sendKeys(password);

        loginButton.click();
    }
}
