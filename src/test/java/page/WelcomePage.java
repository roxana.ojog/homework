package page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class WelcomePage {
    @FindBy(how = How.ID, using = "user")
    private WebElement usernameTextElement;

    public String getUsernameText() {
        return usernameTextElement.getText();
    }
}
