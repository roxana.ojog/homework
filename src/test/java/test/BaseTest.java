package test;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
    WebDriver driver;

    @BeforeMethod
    public void baseSetUp() {
        driver = WebBrowser.getBrowser();
    }

    @AfterMethod
    public void cleanUp() {
        this.driver.quit();
    }
}
