package test;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page.LoginPage;
import page.WelcomePage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class LoginTest extends BaseTest {
    private final String URL = "http://siit.epizy.com/web-stubs/stubs/auth.html";
    LoginPage loginPage;

    @BeforeMethod
    public void setUp() {
        driver.get(URL);
        loginPage = PageFactory.initElements(driver, LoginPage.class);
    }

    @DataProvider(name = "loginDataProvider")
    public Iterator<Object[]> loginDataProvider() {
        Collection<Object[]> dataProvider = new ArrayList<>();

        dataProvider.add(new String[]{"dinosaur", "dinosaurpassword"});
        dataProvider.add(new String[]{"dingo", "dingopassword"});
        dataProvider.add(new String[]{"camel", "camelpassword"});
        dataProvider.add(new String[]{"zebra", "zebrapassword"});

        return dataProvider.iterator();
    }

    @Test(dataProvider = "loginDataProvider")
    public void loginTest(String username, String password) {
        loginPage.login(username, password);

        WelcomePage welcomePage = PageFactory.initElements(driver, WelcomePage.class);
        Assert.assertEquals(welcomePage.getUsernameText(), username);
    }

    @DataProvider(name = "loginUnsuccessfulDataProvider")
    public Iterator<Object[]> loginUnsuccessfulDataProvider() {
        Collection<Object[]> dataProvider = new ArrayList<>();

        dataProvider.add(new String[]{"dinosaur", "", "", "Please enter your password", ""});
        dataProvider.add(new String[]{"", "dcvdvdvdv", "Please enter your username", "", ""});
        dataProvider.add(new String[]{"un_user", "dcvdvdvdv", "", "", "Incorrect username or password"});
        dataProvider.add(new String[]{"", "", "Please enter your username", "Please enter your password", ""});

        return dataProvider.iterator();
    }

    @Test(dataProvider = "loginUnsuccessfulDataProvider")
    public void loginUnsuccessfulTest(String username, String password, String usernameError, String passwordError, String generalError) throws InterruptedException {
        loginPage.login(username, password);

        Assert.assertEquals(loginPage.getUsernameErrorText(), usernameError);
        Assert.assertEquals(loginPage.getPasswordErrorText(), passwordError);
        Assert.assertEquals(loginPage.getGeneralErrorText(), generalError);
    }
}
