package test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.List;

public class MyFirstTest extends BaseTest {

    @Test
    public void myFirstTest() throws InterruptedException {
        driver.get("http://google.com");
        WebElement sb = driver.findElement(By.name("q"));
        sb.sendKeys("koala", Keys.RETURN);

        WebElement resSummary = driver.findElement(By.id("resultStats"));
        String elementText = resSummary.getText();
        System.out.println(elementText);

        List<WebElement> resultItems = driver.findElements(By.cssSelector("div.r > a:not(.fl)"));

        System.out.println(resultItems.size());

        for (WebElement we : resultItems) {
            WebElement titleElement = we.findElement(By.cssSelector("h3 span"));
            // print title from we
            System.out.println(titleElement.getText());
            // print url from we
            System.out.println(we.getAttribute("href"));
        }

        Assert.assertTrue(resultItems.size() > 0);
    }
}
