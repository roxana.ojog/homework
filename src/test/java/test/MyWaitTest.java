package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class MyWaitTest extends BaseTest {
    private final String URL = "http://siit.epizy.com/web-stubs/stubs/lazy.html";

    @BeforeMethod
    public void beforeMethod() {
        driver.get(URL);
    }

    @Test
    public void noWaitIShouldFail() {
        WebElement element = driver.findElement(By.id("lazy-button"));
        element.click();
        element = driver.findElement(By.id("lazy-button"));
        Assert.assertTrue(element.isDisplayed());
    }

    @Test
    public void implicitWait() {
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        WebElement element = driver.findElement(By.id("lazy-button"));
        element.click();
        element = driver.findElement(By.id("lazy-button"));
        Assert.assertTrue(element.isDisplayed());
    }

    @Test
    public void explicitWait() {
        WebElement element = driver.findElement(By.id("lazy-button"));
        element.click();

        WebDriverWait wait = new WebDriverWait(driver, 20);
        /**
         * WebDriverWait will call the ExpectedConditions
         * every 500 milliseconds
         */
        element = wait.until(
                ExpectedConditions.presenceOfElementLocated(By.id("lazy-button")));
        Assert.assertTrue(element.isDisplayed());
    }
}
