package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page.RegistrationPage;

public class NegativeRegistration extends BaseTest {
    private final String URL = "http://siit.epizy.com/web-stubs/stubs/auth.html";
    RegistrationPage registrationPage;

    @BeforeMethod
    public void setUp() {
        driver.get(URL);
        RegistrationPage = PageFactory.initElements(driver, RegistrationPage.class);

            @Test
            public void emptyName() {
                WebElement userName = driver.findElement(By.name("first_name"));
                userName.sendKeys("roxana");

                WebElement fullName = driver.findElement(By.name("last_name"));
                fullName.sendKeys("ojog");
                WebElement email = driver.findElement(By.name("email"));
                email.sendKeys("test@yahoo.com");
                WebElement password = driver.findElement(By.name("password"));
                password.sendKeys("test@blab233");

                WebElement termsOfServices = driver.findElement(By.id("terms_of_service"));
                termsOfServices.click();
                WebElement signUp = driver.findElement(By.xpath("//button[contains(@class,'btn sign-up-btn-2 btn-block')]"));
                signUp.click();
                String expectedErrorMsg = "Please enter your Name";
                WebElement exp = driver.findElement(By.xpath("//p[contains(text(),'Please enter your Name')]"));
                String actualErrorMsg = exp.getText();
                Assert.assertEquals(actualErrorMsg, expectedErrorMsg);
            }
        }
    }
