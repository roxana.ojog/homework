package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.internal.junit.ArrayAsserts;
import page.LoginPage;
import page.RegistrationPage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class RegistrationTest extends BaseTest {
    private final String URL = "http://siit.epizy.com/web-stubs/stubs/auth.html";
    RegistrationPage registrationPage;

    @BeforeMethod
    public void setUp() {
        driver.get(URL);
        RegistrationPage = PageFactory.initElements(driver, RegistrationPage.class);
    }

    @DataProvider(name = "registrationDataProvider")
    public Iterator<Object[]> registrationDataProvider() {
        Collection<Object[]> dataProvider = new ArrayList<>();

        dataProvider.add(new String[]{"name", "dinosaurpassword"});
        dataProvider.add(new String[]{"dingo", "dingopassword"});
        dataProvider.add(new String[]{"camel", "camelpassword"});
        dataProvider.add(new String[]{"zebra", "zebrapassword"});

        return dataProvider.iterator();
    }
    @Test
    public void validRegistrationTest(){

        WebElement userName = driver.findElement(By.name("firstname"));
        userName.sendKeys("roxana");

        WebElement fullName = driver.findElement(By.name("name"));
        fullName.sendKeys("ojog");

        WebElement email = driver.findElement(By.name("email"));
        email.sendKeys("ojog.roxana@yahoo.com");

        WebElement password = driver.findElement(By.name("password"));
        password.sendKeys("Test@12345");

        WebElement phone = driver.findElement(By.name("username"));
        phone.sendKeys("roxana.ojog");

        WebElement signUp = driver.findElement(By.xpath("//button[contains(@class,'btn btn-primary')]"));
        signUp.click();

        String expectedURL = "http://siit.epizy.com/web-stubs/stubs/auth.html";
        String actualURL = driver.getCurrentUrl();
        Assert.assertEquals(actualURL, expectedURL);

        String expectedTitle = "Verify Your Email Address - roxana.ojog ";
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, expectedTitle);

    }

}
